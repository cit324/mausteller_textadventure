﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public NavigationController nav; //updated to public
    private InputController ui;
    public List<string> inventory;

    public static void Save() => instance.SaveSelf();
    public static void Load() => instance.LoadSelf();
    public static void ResetSave() 
    {
        instance.ClearInventory();
        instance.nav.ResetRoom();
    }
    
    private void Awake() //classic singleton structure
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        inventory = new List<string>();
        nav = GetComponent<NavigationController>();
        ui = GetComponent<InputController>();
        LoadSelf();
        ui.UpdateDisplayText(nav.Unpack()); //get the room description and display
        ui.onGameOver += ClearInventory;
    }

    internal void ClearInventory()
    {
        inventory.Clear();
        Debug.Log("Inventory cleared");
    }

    private void SaveSelf()
    {
        SaveState theData = new SaveState();
        theData.currentRoom = nav.currentRoom.name; //string version of the so (just the name)
        theData.inventory = instance.inventory;

        BinaryFormatter bf = new BinaryFormatter();
        Debug.Log(Application.persistentDataPath);
        FileStream fileStream = File.Create(Application.persistentDataPath + "/player.save");
        bf.Serialize(fileStream, theData);
        fileStream.Close();
    }

    private void LoadSelf()
    {
        //check to see if they have saved previously
        if(File.Exists(Application.persistentDataPath + "/player.save"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fileStream = File.Open(Application.persistentDataPath + "/player.save", FileMode.Open);
            SaveState theData = (SaveState) bf.Deserialize(fileStream);
            fileStream.Close();

            nav.currentRoom = nav.GetRoomByName(theData.currentRoom);
            instance.inventory = theData.inventory ?? new List<string>();
        }
    }
}
