﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public InputField inputField;
    public Toggle mode;
    public Toggle bigFont;
    public Text displayText; //reference to large text area

    private List<string> valid = new List<string>
    {
        "help",
        "go",
        "get",
        "restart",
        "save",
        "inventory"
    }; //our valid commands
    public Image background;
    public Text toggleLabel;
    public Text bigFontLabel;
    public Text placeHolder;
    public Text inputFieldText;

    private string story;
    private bool darkMode; //if true - darkmode is on; if false darkmode is off
    private bool bigFontMode;

    public delegate void GameOver(); //set up the delegate and template

    public event GameOver onGameOver; //create event linked to delegate

    void Start()
    {
        //UpdateDisplayText("You wake up in a dark room."); //added
        inputField.onEndEdit.AddListener(GetInput);
        mode.onValueChanged.AddListener(ModeChange);
        bigFont.onValueChanged.AddListener(BigFontChange);

        //load saved preferences correctly
        if (PlayerPrefs.HasKey("DarkMode"))
        {
            int isDark = PlayerPrefs.GetInt("DarkMode");
            darkMode = isDark == 1;
            SetTheme();
        }
        else //key not found
        {
            PlayerPrefs.SetInt("DarkMode", 1); //by default the dark mode is enabled
            PlayerPrefs.Save();
        }

        if (PlayerPrefs.HasKey("BigFont"))
        {
            bigFont.isOn = bigFontMode = PlayerPrefs.GetInt("BigFont") == 1;
        }
        else
        {
            PlayerPrefs.SetInt("BigFont", 1);
            PlayerPrefs.Save();
        }
    }

    //notifies of change in preference
    void ModeChange(bool userInput)
    {
        //player pref
        darkMode = userInput;
        PlayerPrefs.SetInt("DarkMode", darkMode ? 1 : 0);
        PlayerPrefs.Save();
        SetTheme();
    }

    void BigFontChange(bool userInput)
    {
        bigFontMode = userInput;
        PlayerPrefs.SetInt("BigFont", bigFontMode ? 1 : 0);
        PlayerPrefs.Save();
        SetBigFont();
    }

    void SetBigFont()
    {
        if (!bigFontMode)
        {
            displayText.fontSize = 24;
        }
        else
        {
            displayText.fontSize = 36;
        }
    }

    void SetTheme()
    {
        if (darkMode)
        {
            background.color = Color.black;
            displayText.color = Color.white;
            toggleLabel.color = Color.white;
            bigFontLabel.color = Color.white;
            placeHolder.color = Color.white;
            inputFieldText.color = Color.white;
            mode.isOn = true;
        }
        else
        {
            background.color = Color.white;
            displayText.color = Color.black;
            toggleLabel.color = Color.black;
            bigFontLabel.color = Color.black;
            placeHolder.color = Color.black;
            inputFieldText.color = Color.black;
            mode.isOn = false;
        }
    }

    void GetInput(string userInput)
    {
        inputField.text = ""; // clear user input
        inputField.ActivateInputField(); // put user back in input field (so they don't have to click!)

        if (userInput != "") //not empty
        {
            char[] splitInfo = {' '}; //delimeter
            string rawCommands = userInput.Trim().ToLower();
            //Debug.Log(rawCommands);
            string[] inputCommands = rawCommands.Split(splitInfo); //break user input up
            //Debug.Log(inputCommands[0]);
            if (valid.Contains(inputCommands[0]))
            {
                //is this a valid command
                Debug.Log(userInput + " found");
                UpdateDisplayText(userInput);
                switch (inputCommands[0])
                {
                    case "help":
                        UpdateDisplayText(
                            "go = Use this command with the direction of either North, South, West, or East." +
                            "\nget = Use with the name of the object you are attempting to pickup." +
                            "\nsave = Use this in order to save your current place in the game." +
                            "\ninventory = Shows list of items in inventory." +
                            "\nrestart = Use this to restart the game.");
                        break;
                    case "inventory":
                        string i = "";
                        for (var k = 0; k < GameManager.instance.inventory.Count; k++)
                        {
                            i += GameManager.instance.inventory[k];
                            if (k < GameManager.instance.inventory.Count - 1)
                                i += ", ";
                        }
                        UpdateDisplayText(i);
                        break;
                }

                if (inputCommands[0] == "go")
                {
                    bool result = GameManager.instance.nav.SwitchRoom(inputCommands[1]);

                    if (result) //if true - switched to a valid room
                    {
                        UpdateDisplayText(GameManager.instance.nav.Unpack());
                        if (GameManager.instance.nav.GetNumExits() == 0)
                        {
                            UpdateDisplayText("Type 'Restart' to Restart");
                            if (onGameOver != null) //are any functions connected -- is anyone listening?
                                onGameOver();
                        }
                    }
                    else
                        UpdateDisplayText("Sorry, there is no exit in that direction or the door is locked");
                }
                else if (inputCommands[0] == "restart")
                {
                    GameManager.ResetSave();
                    //PlayerPrefs.DeleteAll();
                    //PlayerPrefs.Save();
                    GameManager.Save();
                    SceneManager.LoadScene(0);
                } //lowercase!!!
                    
                else if (inputCommands[0] == "get")
                {
                    // get the key and add it to inventory
                    if (GameManager.instance.nav.TakeItem(inputCommands[1]))
                    {
                        //add to inventory
                        GameManager.instance.inventory.Add(inputCommands[1]);
                        UpdateDisplayText(inputCommands[1] + " item has been added to the inventory");
                    }
                    else
                        UpdateDisplayText("That item is not in this room. Please try again");
                }
                else if (inputCommands[0] == "save")
                {
                    //call to save function
                    GameManager.Save();
                }
                //UpdateDisplayText(userInput); //moved this up
            }
            else
            {
                Debug.Log(userInput + " not found");
                UpdateDisplayText("Command not found");
            }
        }

        //Debug.Log(userInput);
    } //end of GetInput function

    public void UpdateDisplayText(string msg)
    {
        story += "\n" + msg;
        displayText.text = story;
    }
}